all: hwc.c hwvm.c
	gcc hwc.c -o hwc
	gcc hwvm.c -o hwvm

debug: hwc.c hwvm.c
	gcc -g hwc.c -o hwc
	gcc -g hwvm.c -o hwvm

clean:
	rm hwc
	rm hwvm
