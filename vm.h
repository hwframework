#ifndef VM_H
#define VM_H

#include <stdint.h>

#define HW_HEADER_MAGIC 0xA4
#define HW_HEADER_VERSION 0x10
#define LAST_INSTRUCTION 3

typedef struct hw_header {
    uint8_t  hw_magic;
    uint8_t  hw_version;
    uint16_t hw_instr_count;
} HwHeader;

typedef struct hw_instr {
    int          i_char;
    uint8_t      i_opcode;
    const char  *i_data; 
} HwInstruction;

HwInstruction instructions[] = {
    {
        .i_char     = 'h',
        .i_opcode   = 0x1,
        .i_data     = "Hello, world\n"
    },
    {
        .i_char     = 'H',
        .i_opcode   = 0x2,
        .i_data     = "HELLO, WORLD!\n"
    },
    {
        .i_char     = 'v',
        .i_opcode   = 0x3,
        .i_data     = "Hell�, vil�g!\n"
    },
    {
        .i_char     = 0,
        .i_opcode   = 0x0,
        .i_data     = (void *)0 
    }
};

#endif
