/* Hello world program compiler */
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"

static int parse(FILE *, FILE *);
static void write_hw_header(FILE *, int);
static HwInstruction *get_instr(int);
static void compiler_error(const char *, ...);
static int is_comment(int);

int
main(int argc, char *argv[])
{
    FILE *in, *out;
    int in_count;
    char *file_name = NULL;
    if (argc == 1) 
       compiler_error("No input files!\n");

    file_name = argv[1];
    in = fopen(file_name, "rt");
    if (in == NULL) 
        compiler_error("Cannot open file %s\n", file_name);

    file_name[strlen(file_name)-1] = 'x';
    out = fopen(file_name, "wb");
    if (out == NULL)
        compiler_error("Cannot open file %s\n", file_name);

    fseek(out, sizeof(HwHeader), SEEK_SET);
    in_count = parse(in, out);
    write_hw_header(out, in_count);

    fclose(in);
    fclose(out);
    return 0;
}

static int
parse(FILE *in, FILE *out)
{
    int input;
    int i, count = 0;
    HwInstruction *inst = NULL;

    while (!feof(in)) {
        fscanf(in, "%c", (char *)&input);
        
        if (is_comment(input))
            continue;

        if (!isalpha(input))
            continue;

        inst = get_instr(input);
        if (inst == NULL)
            continue;

        fwrite((void *)&inst->i_opcode, sizeof(uint8_t), 1, out);
        count++;
    }
    return count;
}

static void
write_hw_header(FILE *out, int count)
{
    HwHeader head = {
           .hw_magic = HW_HEADER_MAGIC,
           .hw_version = HW_HEADER_VERSION,
           .hw_instr_count = count
    };

    fseek(out, 0L, SEEK_SET);
    fwrite(&head, sizeof(HwHeader), 1, out);
}

static HwInstruction * 
get_instr(int inst)
{
    int i;
    HwInstruction *hi = NULL;
    for (i = 0; i < LAST_INSTRUCTION; i++) {
       if (instructions[i].i_char == inst) {
           hi = &instructions[i];
           return hi;
       }
    }
    return NULL;
} 

static void
compiler_error(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "HW Compiler - (C) Akos Kovacs\n\nError: ");
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    exit(1);
}

static int
is_comment(int c)
{
    static int comment = 0;

    switch (c) {
        case '#' :
        comment = 1;
        return 1;

        case '\n' :
        comment = 0;
        return 1;

        default:
        if (comment)
            return 1;
        else 
            return 0;
    }
}
