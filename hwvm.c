#include "vm.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

static void start_vm(FILE *, const HwHeader *);
static void execute_opcode(uint8_t);
static void vm_error(const char *, ...);

int
main(int argc, char **argv)
{
    FILE *file;
    HwHeader head;

    if (argc == 1)
       vm_error("No executable file!\n");

    file = fopen(argv[1], "rt");
    if (file == NULL)
       vm_error("Cannot read file %s\n", argv[1]);

    fread(&head, sizeof(HwHeader), 1, file);

    if (head.hw_magic != HW_HEADER_MAGIC)
        vm_error("File is corrupted or not a valid application!\n");

    start_vm(file, &head);
    return 0;
}

static void
start_vm(FILE *file, const HwHeader *head)
{
    uint8_t *text_segment;
    int i;
    text_segment = (uint8_t *)malloc(sizeof(uint8_t) * head->hw_instr_count);

    if (text_segment == NULL) 
        vm_error("Cannot allocate text segment\n");

    fread(text_segment, sizeof(uint8_t), head->hw_instr_count, file);

    for (i = 0; i < head->hw_instr_count; i++) {
        execute_opcode(text_segment[i]);
    }

}
static void
execute_opcode(uint8_t opcode)
{
    HwInstruction *inst;
    int i;

    for (i = 0; i < LAST_INSTRUCTION; i++) {
        inst = &instructions[i];
        if (opcode == inst->i_opcode) {
            printf("%s", inst->i_data);
        }
    }
}

static void
vm_error(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    fprintf(stderr, "HW Virtual Machine - (C) Akos Kovacs\n\nError: ");
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    exit(1);
}
